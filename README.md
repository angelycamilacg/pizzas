![Pizzería](http://www.madarme.co/portada-web.png) 
# Título del proyecto: Pizzería 🍕

En este proyecto se realiza un algortimo para calcular la factura de un pedido de pizzas.

## Tabla de contenido 📋

1. [Características](#características)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)


## Características 📝

- Proyecto con lectura de datos JSON 
- Carga dinámica del JSON
- Archivo de JSON: [ver](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json)
- Uso del método URLSearchParams para la lectura de datos enviados por la URL

## Contenido del proyecto 📄
[pizzeria.html](https://gitlab.com/angelycamilacg/pizzas/-/blob/master/pizzeria.html)

- Archivo principal de invocación a la lectura de JSON. 
- En este archivo se lee la cantidad de pizzas que el usuario digita; según dicha cantidad, se muestran los tamaños de las pizzas (grande, mediana, pequeña). 

[opciones.html](https://gitlab.com/angelycamilacg/pizzas/-/blob/master/html/opciones.html)

- Archivo donde se muestran diferentes opciones para que el usuario escoja el sabor de la pizza (puede escoger uno o dos) y los ingredientes adicinoales para cada sabor.

[factura.html](https://gitlab.com/angelycamilacg/pizzas/-/blob/master/html/factura.html)

- Archivo donde se muestra la tabla con el tamaño, los sabores y los ingredientes adicionales seleccionados por el usuario, el precio de cada pizza y el total.

[pizzeria.js](https://gitlab.com/angelycamilacg/pizzas/-/blob/master/js/pizzeria.js)

- Archivo JS con el proceso de lectura del JSON y sus funciones adicionales donde se leen los sabores de las pizzas, los ingredientes adicionales y se muestran las imagenes del sabor seleccionado.

[style.css](https://gitlab.com/angelycamilacg/pizzas/-/blob/master/css/style.css)

- Archivo donde se da estilo a la página.

## Tecnologías 💻

- HTML5
- JavaScript
- CSS3

## IDE 🌐

- El proyecto se desarrolla usando sublime text 3 
- Visor de JSON -(http://jsonviewer.stack.hu/)

## Instalación 🖥
Firefox Developer Edition: [descargar](https://www.mozilla.org/es-ES/firefox/developer/) Software necesario para ver la interacción por consola y depuración del código JS.

- **Descargar** proyecto
- **Invocar** página pizzeria.html desde firefox


## Demo 🌐

Para ver el demo de la aplicación puede dirigirse a: [Pizzeria](http://ufps18.madarme.co/Pizzeria/)

## Autor(es) 👩‍👩‍

Proyecto desarrollado por:

- Yurley Gabriela Espinel Santos - yurleygabrielaes@ufps.edu.co
- Angely Camila Calderón García - angelycamilacg@ufps.edu.co

## Institución Académica 

Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Marco Adarme]: <http://madarme.co>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>

   